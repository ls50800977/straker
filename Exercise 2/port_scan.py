#!/bin/python

import socket
import sys

ip = sys.argv[1]

for port in [25, 80, 8080, 8999]:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    r = s.connect_ex((ip, port))
    if (r == 0):
        print("port " + str(port) + " open")
    else:
        print("port " + str(port) + " closed")
